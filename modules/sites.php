<?php
use model\shops as shops;
use model\rtask as remoteTask;
use model\rProduct as rProduct;
$id=$this->param[0];
$shop=new shops();
$pl=new rProduct();
foreach ($shop->get($id)as $row){
    print_r($row);
    $mod_name='Working with: '.$row->ShopName;
    $rt=new remoteTask();
    $tl=$rt->getAll($row->ApiUrl);
    $pli=$pl->getAll($row->ApiUrl);
    $_SESSION['ApiUrl'] = $row->ApiUrl;
}
$plist='';
foreach($pli as $opt){
    $plist.='<option value="'.$opt->products_id.'">'.$opt->products_model.'</option>';
}
$tpl='admin';
$out="<a class='btn btn-primary' data-toggle='modal' data-target='#myModal'><i class='fa fa-plus'></i> Add task</a><hr>";
$modal_name='Add task';
$modal_content='            <form method="POST" id="post_form" action="'.WWW_BASE_PATH.'task/add" >
                            <input type="hidden" name="task_id">
                            <label>models</label>
                            <select name="product_id" id="popt" onchange="setModel(this)">
                            '.$plist.'
                            </select>
                            <input type="hidden" name="models" id="model"><br>
                            <label>agenda</label><input name="agenda"><br>
                            <label>howManyTimes</label><input name="howManyTimes"><br>
                            <label>place</label><input name="place"><br>
                            <label>task_status</label><input name="task_status"><br>
                            <input type="hidden" name="token" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IntcInN1YlwiOlwiMTIzNDU2Nzg5MFwiLFwibmFtZVwiOlwibWVnYXplblwiLFwiaWF0XCI6MTUxNjIzOTAyMn0i.9l7pSy1vXrJRIsIG50u7E_8t7ZJIDIeThZDjWxPwC_0">
                            <button class="btn btn-primary" type="submit">Create task</button>
                        </form>
                        ';

$Table="<table class='table table-bordered table-striped'><tr>
    <thead>
    <tr>
    <td>id</td>
    <th>Models</th>
    <th>Agenda</th>
    <th>Place</th>
    <th>Times</th>
    <th>Status</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    ";
foreach ($tl as $tt){

$Table.="<tr>
    <td>$tt->task_id</td>
    <td>$tt->models</td>
    <td>$tt->agenda</td>
    <td>$tt->place</td>
    <td>$tt->howManyTimes</td>
    <td>$tt->task_status</td>
    <td>
    <a href='".WWW_BASE_PATH."task/edit/$tt->task_id' class='btn btn-outline-primary'><i class='fa fa-edit'></i></a>
    <a href='#' data-href='".WWW_BASE_PATH."task/delete/$tt->task_id' data-toggle='modal' data-target='#confirm-delete' class='btn btn-outline-primary'><i class='fa fa-trash-o'></i></a>    
    </td>
    </tr>";
}
$Table.="</tbody></table>";
$out.=$Table;
include TEMPLATE_DIR.DS.$tpl.".html";