<?php
use model\reviewFeed;
$limit=10;
$tpl='admin';
$mod_name ="Review";
$tb=new reviewFeed();

if($this->param[0]=="page"){
    $page=$this->param[1];
    
}else{
    $page=0;
}
$paginator='';
$start_from=$page*$limit;
$tdata=$tb->ShowPaginated("LIMIT $start_from, $limit");
$total_records=$tb->found;
$total_pages = ceil($total_records / $limit);
$cur=$page+1;
$next=$cur;
$prev=$page-1;
if($cur>=2){
$paginator.="<li class='page-item'><a class='btn' href='".WWW_BASE_PATH."reviews/page/0'>&lt;&lt;</a></li>";
$paginator.="<li class='page-item'><a class='btn' href='".WWW_BASE_PATH."reviews/page/".$prev."'>&lt;</a></li>";
    }else{
}


$paginator.="<li class='page-item'><a class='btn' href='".WWW_BASE_PATH."reviews/page/".$page."'>".$cur."</a></li>";
if($cur>=$total_pages){}else{
$paginator.="<li class='page-item'><a class='btn' href='".WWW_BASE_PATH."reviews/page/".$next."'>&gt;</a></li>";
$paginator.="<li class='page-item'><a class='btn' href='".WWW_BASE_PATH."reviews/page/".$total_pages."'>&gt;&gt;</a></li>";
}
$out="page". ($page+1) ."from $total_pages";
$out.="<ul class='pagination justify-content-end'>"
        .$paginator."
      </ul>
      
    <table class='table table-striped table-bordered table-sm table-hover'>
    <tr>
    <th>id</th>
    <th>model</th>
    <th>category</th>
    <th>review</th>
    <th>reviews_rating</th>
    <th>country</th>
    <th>town</th>
    <th>customers_name</th>
    <th>time</th>
    <th>seed</th>
    <th>status</th>
    <th>action</th></tr>";
foreach($tdata as $row){
    //$out= print_r($row,TRUE);
    $out.="<tr>"
            . "
    <td>$row->id</td>
    <td>$row->model</td>
    <td>$row->category</td>
    <td>$row->review</td>
    <td>$row->reviews_rating</td>
    <td>$row->country</td>
    <td>$row->town</td>
    <td>$row->customers_name</td>
    <td>$row->time</th>
    <td>$row->seed</th>
    <td>$row->status</td><td>"
            . "<div class='btn-group'>"
            . "<a href='".WWW_BASE_PATH."reviews/edit/$row->id' class='btn btn-outline-primary'><i class='fa fa-edit'></i></a>"
            . "<a href='#' data-href='".WWW_BASE_PATH."reviews/delete/$row->id' data-toggle='modal' data-target='#confirm-delete' class='btn btn-outline-primary'><i class='fa fa-trash-o'></i></a>"
            . "</div>"
            . "</td></tr>";
}
$out.="</table>";
    
include TEMPLATE_DIR.DS.$tpl.".html";