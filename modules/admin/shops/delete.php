<?php
use model\shops as shops;
$id=$this->param[0];
//header("HTTP/1.1 201 success!");
$shops= new shops();
$returnPath=$_COOKIE['referer'];
$shops->del($id);
if(!$shops->lastState){
    setcookie('message',"<div id='flash-msg' class='alert alert-success'>
    <button type='button' class='close' data-dismiss='alert'onclick='$.removeCookie(\"message\");'>&times;</button>
  <strong>Shop aded!</strong>.
</div>");
} else {
setcookie("message", "<div id='flash-msg' class='alert alert-danger'>
    <button type='button' class='close' data-dismiss='alert'onclick='$.removeCookie(\"message\");'>&times;</button>
  <strong>Shop not aded!</strong>.".$shops->lastState."
</div>");
}
header("Location: ".$returnPath);
exit();