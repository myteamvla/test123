<?php
use model\shops as shops;
$shops= new shops();
if(!$_POST){
    
    setcookie("message",null,-1, '/');
    $tpl="admin";
    $id=$this->param[0];
    $f=$shops->get($id);
    $f=$f[0];
    //print_r($f);
    $out='
<form method="POST">
  <div class="form-group row">
  <input id="id" name="id" type="hidden" value="'.$f->id.'"> 
    <label for="identy" class="col-3 col-form-label">Shop Identy</label> 
    <div class="col-9">
      <input id="identy" name="identy" value="'.$f->identy.'" placeholder="identy" aria-describedby="identyHelpBlock" required="required" class="form-control here" type="text"> 
      <span id="identyHelpBlock" class="form-text text-muted">Sequire key for Api authorization</span>
    </div>
  </div>
  <div class="form-group row">
    <label for="ShopName" class="col-3 col-form-label">Shop Name</label> 
    <div class="col-9">
      <input id="ShopName" name="ShopName" value="'.$f->ShopName.'" placeholder="Shop Name" aria-describedby="ShopNameHelpBlock" required="required" class="form-control here" type="text"> 
      <span id="ShopNameHelpBlock" class="form-text text-muted">Shop Name display in select shop menu</span>
    </div>
  </div>
  <div class="form-group row">
    <label for="ApiUrl" class="col-3 col-form-label">Shop API Url</label> 
    <div class="col-9">
      <input id="ApiUrl" name="ApiUrl" value="'.$f->ApiUrl.'" placeholder="ApiUrl" aria-describedby="ApiUrlHelpBlock" required="required" class="form-control here" type="text"> 
      <span id="ApiUrlHelpBlock" class="form-text text-muted">Base URL, there located Shop API</span>
    </div>
  </div> 
  <div class="form-group row">
    <div class="offset-3 col-9">
      <button name="submit" type="submit" class="btn btn-primary">Save</button>
    </div>
  </div>
</form>
';
    include TEMPLATE_DIR.DS.$tpl.".html";

}else{
$param=($_POST);
print_r($param);
$shops->save($param);
$returnPath=WWW_ADMIN_PATH.'shops/';

if(!$shops->lastState){
    setcookie('message',"<div id='flash-msg' class='alert alert-success'>
    <button type='button' class='close' data-dismiss='alert'onclick='$.removeCookie(\"message\");'>&times;</button>
  <strong>Shop saved!</strong>.
</div>");
} else {
setcookie("message", "<div id='flash-msg' class='alert alert-danger'>
    <button type='button' class='close' data-dismiss='alert'onclick='$.removeCookie(\"message\");'>&times;</button>
  <strong>Shop not save!</strong>.".$shops->lastState."
</div>");
}
header("Location: ".$returnPath);
exit();

}