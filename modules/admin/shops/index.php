<?php
$tpl='admin';
//default var
$mod_name="Manage shop list";
$modal_name='Add Shop';
$modal_content='<form id="w0" action="'.WWW_BASE_PATH.'admin/shops/add" method="post">
<div class="form-group field-shops-identy required">
<label class="control-label" for="shops-identy">Identy</label>
<input id="shops-identy" class="form-control" name="Shops[identy]" value="test1" maxlength="50" aria-required="true" type="text">

<div class="help-block"></div>
</div>
    <div class="form-group field-shops-shopname required">
<label class="control-label" for="shops-shopname">Shop Name</label>
<input id="shops-shopname" class="form-control" name="Shops[ShopName]" value="test1" maxlength="50" aria-required="true" type="text">

<div class="help-block"></div>
</div>
    <div class="form-group field-shops-apiurl required">
<label class="control-label" for="shops-apiurl">Api Url</label>
<input id="shops-apiurl" class="form-control" name="Shops[ApiUrl]"  maxlength="50" aria-required="true" type="text">

<div class="help-block"></div>
</div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Save</button>    </div>

    </form>'
        . "";
$sh=new db();
$q="SELECT `id`, `identy`, `ShopName`, `ApiUrl` FROM `shops`";
$out="<button class='btn btn-outline-primary' data-toggle='modal' data-target='#myModal'>"
        . "<i class='fa fa-plus'></i> add shop</button><br>"
        . "        <table class='table table-striped table-bordered table-sm table-hover'>"
        . "<tr class='table-inverse'><th>identy</th><th>ShopName</th><th>ApiUrl</th><th>action</th></tr>";

    
foreach ($sh->get_result($q) as $row) {
    //print_r($row);
    
    $out.="<tr><td>$row->identy</td><td>$row->ShopName</td><td>$row->ApiUrl</td><td>"
            . "<div class='btn-group'>"
            . "<a href='".WWW_ADMIN_PATH."shops/edit/$row->id' class='btn btn-outline-primary'><i class='fa fa-edit'></i></a>"
            . "<a href='#' data-href='".WWW_ADMIN_PATH."shops/delete/$row->id' data-toggle='modal' data-target='#confirm-delete' class='btn btn-outline-primary'><i class='fa fa-trash-o'></i></a>"
            . "</div>"
            . "</td></tr>";
};
$out.="</table>";
include TEMPLATE_DIR.DS.$tpl.".html";