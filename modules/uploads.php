<?php
ini_set('upload_max_filesize','30M');
ini_set('post_max_size','30M');
//use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
$allowedExtensions = array('xlsx');
$uploadDirectory = APP_PATH.DS."upload".DS;

    $errors = []; // Store all foreseen and unforseen errors here

    $fileExtensions = 'xlsx'; // Get all the file extensions
    //print_r($_FILES);
    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileTmpName  = $_FILES['myfile']['tmp_name'];
    $fileType = $_FILES['myfile']['type'];
    $fileE =pathinfo($fileName);
    $fileExtension =$fileE['extension'];

    $uploadPath = $uploadDirectory . "import.xlsx"; 

    //echo $uploadPath;

    if (isset($_POST)) {

        if ($fileExtension!=$fileExtensions) {
            $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
        }
      
        if ($fileSize > 2000000) {
            $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
        }

        if (empty($errors)) {
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

            if ($didUpload) {
                echo "The file " . basename($fileName) . " has been uploaded";
            } else {
                echo "An error occurred somewhere. Try again or contact the admin";
            }
        } else {
            foreach ($errors as $error) {
                echo $error . "These are the errors" . "\n";
            }
        }
    

$inputFileName = $uploadPath;

$reader = new Xlsx();
$spreadsheet = $reader->load($inputFileName);

$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
echo"<pre>";

$rew=new model\reviewFeed();
$i=0;
foreach($sheetData as $r){
    
    if(isset($r['B'])){
        if($r['B']!='Name'){
        $rew->add($r);
        $i++;
        }
    } else {
        break;    
    }
}
setcookie("message","<div id='flash-msg' class='alert alert-success'>
    <button type='button' class='close' data-dismiss='alert'onclick='$.removeCookie(\"message\");'>&times;</button>
  <strong>Imported $i records</strong>.
</div>");
header("Location: ".$returnPath);
        }
?>