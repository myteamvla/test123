<?php

namespace model;

class rtask {

    private $apiVer = "v1";

    public function getAll($url) {
        $uri = $url .  $this->apiVer . "/task";
        $response = \Httpful\Request::get($uri)
                ->expectsJson()
                ->sendIt();
        return $response->body;
    }

    public function getOne($url, $id) {
        $uri = $url .  $this->apiVer . "/task/" . $id;
        $response = \Httpful\Request::get($uri)
                ->expectsJson()
                ->sendIt();
        $z=$response->body;
        return $z[0];
    }
    public function newTask($url, $id) {
        $uri = $url .  $this->apiVer . "/task/" . $id;
        $response = \Httpful\Request::post($uri)
                ->expectsJson()
                ->sendIt();
        return $response->body;
    }
    public function editTask($url, $id) {
        $uri = $url .  $this->apiVer . "/task/" . $id;
        $response = \Httpful\Request::put($uri)
                ->expectsJson()
                ->sendIt();
        return $response->body;
    }
    public function delTask($url, $id) {
        $uri = $url .  $this->apiVer . "/task/" . $id;
        
        $response = \Httpful\Request::delete($uri)
                ->withAutoParsing()
                ->sendIt();
        return $response->body;
    }

}
