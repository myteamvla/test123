<?php


namespace model;


class rProduct {
   private $apiVer = "v1";
   public function getAll($url) {
        $uri = $url .  $this->apiVer . "/products";
        $response = \Httpful\Request::get($uri)
                ->expectsJson()
                ->sendIt();
        return $response->body;
        $a=new \log2file;
        $a->log($uri, "remote.log");
    }
}
