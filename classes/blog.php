<?php
/**
 * Autogenerated class blog
 * @author merly2k@gmail.com
 */
class blog extends db {

    function __construct() {
	parent::__construct();
    }

    function Get_stat() {
	$q = 'SELECT count(t1.id) as publik,sum(t1.publik) as published,@i :=count(t1.id)-sum(t1.publik) as pended '
		. 'FROM blog t1 '
		. 'JOIN (SELECT @i := 0) var;';
	$tt=$this->get_result($q);
	return $tt[0];
    } 

    /**
     * @param type $id,
     * @param type $name,
     * @param type $article,
     * @param type $publik,
     * @param type $razdel,
     * @param type $pdate
     */
    function Insert($val = array()) {
        foreach ($val as $k => $v) {
            $$k=$v;
        }
	$q = "INSERT INTO `blog` (`name`,`title`, `article`, `publik`, `razdel`, `pdate`) VALUES ('$name','$title', '$article', '$publik', '$razdel', '$pdate');";
	$this->query($q);

	return $this->lastState;
    }

    /**
     * @param type $id
     * @param type $params array(fieldName=>fieldValue)
     */
    function Update($params, $id) {
	$q = "UPDATE `blog` SET ";
	foreach ($params as $k => $v) {
	    $p[] = "`$k`='$v'";
	}
	$q.=implode(", ", $p) . " WHERE  `id`=$id;";
	
	$this->query($q);

	return $this->lastState;
    }

    /**
     * @param type $cel - столбец по которому производится удаление (по умолчанию ID)
     * @param type $val - значение по которому производится удаление
     */
    function Delete($val, $cel = 'id') {
	$q = "DELETE FROM `blog` WHERE  `$cel`='$val;'";

	$this->query($q);

	return $this->lastState;
    }

    /**
     * @param type $cel - столбец по которому производится выборка (по умолчанию ID)
     * @param type $val - значение по которому производится выборка
     */
    function SelectBy($val, $cel = 'id') {
	$q = "SELECT * FROM `blog` WHERE  `$cel`='$val';";
	return $this->get_result($q);
    }

    function SelectAll() {
	$q = "SELECT * FROM `blog` ORDER BY `id` DESC;";
	return $this->get_result($q);
    }

    function SelectAllByRazdel($rzd) {
	$q = "SELECT `id`, `name`,  LEFT(`article`, 256) as `article`, `publik`, `razdel`, `pdate` FROM `blog` WHERE `publik`=1 AND  `razdel`=$rzd ORDER BY `pdate` DESC;";
	return $this->get_result($q);
    }

}

?>