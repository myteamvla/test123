CREATE TABLE IF NOT EXISTS `zen_autoreview` (
	`reviews_id` INT(11) NOT NULL AUTO_INCREMENT,
	`task_id` INT(11) NULL DEFAULT NULL,
	`products_id` VARCHAR(50) NULL DEFAULT NULL,
	`customers_name` VARCHAR(50) NULL DEFAULT NULL,
	`reviews_rating` VARCHAR(50) NULL DEFAULT NULL,
	`date_added` VARCHAR(50) NULL DEFAULT NULL,
	`publish_date` DATETIME NULL DEFAULT NULL,
	`reviews_text` TEXT NULL,
	`status` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`reviews_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;
